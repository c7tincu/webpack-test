'use strict';

module.exports = {

  entry: './src/js/index.js',
  output: {
    path: __dirname + '/dist',
    filename: 'index.js'
  },

  module: {
    loaders: [
      {
        loader: 'jsx-loader?harmony',
        test: /\.jsx?$/
      }
    ],
    postLoaders: [
      {
        loader: 'jshint-loader',
        test: /\.jsx?$/,
        exclude: __dirname + '/node_modules'
      }
    ]
  }

};
