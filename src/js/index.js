'use strict';

var React = require('react');
var Router = require('react-router');

var App = require('./components/App.jsx');
var Contacts = require('./components/Contacts.jsx');
var Section2 = require('./components/Section2.jsx');
var Section3 = require('./components/Section3.jsx');

var DefaultRoute = Router.DefaultRoute;
var Route = Router.Route;

var routes = (
  <Route name="app" path="/" handler={ App }>
    <Route name="section-2" handler={ Section2 }/>
    <Route name="section-3" handler={ Section3 }/>
    <DefaultRoute handler={ Contacts }/>
  </Route>
);

Router.run(routes, (Handler) => {
  React.render(<Handler/>, document.getElementById('app'));
});
