'use strict';

var Logger = {

  getInitialState() {
    this.logger = new window.Logdown({
      prefix: this.constructor.displayName,
      markdown: false
    });
    return null;
  }

};

module.exports = Logger;
