'use strict';

var React = require('react');

var LoggerMixin = require('../mixins/Logger');

var Contacts = React.createClass({

  mixins: [ LoggerMixin ],

  render() {
    this.logger.log('render()');
    return (
      <h1>Contacts</h1>
    );
  }

});

module.exports = Contacts;
