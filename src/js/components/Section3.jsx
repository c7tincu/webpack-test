'use strict';

var React = require('react');

var LoggerMixin = require('../mixins/Logger');

var Section3 = React.createClass({

  mixins: [ LoggerMixin ],

  render() {
    this.logger.log('render()');
    return (
      <h1>Section 3</h1>
    );
  }

});

module.exports = Section3;
