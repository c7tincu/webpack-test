'use strict';

var React = require('react');

var LoggerMixin = require('../mixins/Logger');

var Section2 = React.createClass({

  mixins: [ LoggerMixin ],

  render() {
    this.logger.log('render()');
    return (
      <h1>Section 2</h1>
    );
  }

});

module.exports = Section2;
