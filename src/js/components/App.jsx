'use strict';

var React = require('react');
var Router = require('react-router');

var LoggerMixin = require('../mixins/Logger');

var Link = Router.Link;
var RouteHandler = Router.RouteHandler;

var App = React.createClass({

  mixins: [ LoggerMixin ],

  render() {
    this.logger.log('render()');
    return (
      <div>
        <header>
          <ul>
            <li><Link to="app">Contacts</Link></li>
            <li><Link to="section-2">Section 2</Link></li>
            <li><Link to="section-3">Section 3</Link></li>
          </ul>
        </header>
        <RouteHandler/>
      </div>
    );
  }

});

module.exports = App;
