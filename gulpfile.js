'use strict';

var autoprefixer = require('gulp-autoprefixer');
var gulp = require('gulp');
var plumber = require('gulp-plumber');
var sass = require('gulp-ruby-sass');
var sourcemaps = require('gulp-sourcemaps');
var watch = require('gulp-watch');

gulp.task('sass', function() {
  return sass('src/scss/', {
    compass: true,
    sourcemap: true,
    bundleExec: true,
    style: 'compressed'
  })
    .pipe(plumber())
    .on('error', function(err) {
      console.log(err.toString());
    })
    .pipe(autoprefixer('last 2 versions', '> 2%'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./dist/'));
});

gulp.task('watch', function() {
  watch('src/scss/**/*.scss', { verbose: true }, function() {
    gulp.run('sass');
  });
});



// Configure the default task.
gulp.task('default', function() {
  gulp.run('sass');
});
