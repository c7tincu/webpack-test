webpack-test
============

Build:

```
npm i
bundle install
npm run webpack
npm run gulp
```

Watch styles:

```
npm run gulp -- watch
```

Development server (with hot reload):

```
npm run copy
npm start
```
