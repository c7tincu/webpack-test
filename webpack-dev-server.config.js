'use strict';

module.exports = {

  entry: {
    app: [
      'webpack/hot/dev-server',
      './src/js/index.js'
    ]
  },
  output: {
    path: __dirname + '/dist',
    filename: 'index.js'
  },

  module: {
    loaders: [
      {
        loader: 'jsx-loader?harmony',
        test: /\.jsx?$/
      }
    ],
    postLoaders: [
      {
        loader: 'jshint-loader',
        test: /\.jsx?$/,
        exclude: __dirname + '/node_modules'
      }
    ]
  },

  devServer: {
    host: '0.0.0.0',
    port: 3000
  }

};
